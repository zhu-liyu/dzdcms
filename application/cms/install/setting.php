<?php

/**
 * 内容管理系统，默认配置
 */
return array(
    'domain'             => '',
    'web_site_status'    => 1,
    'web_site_recycle'   => 0,
    'web_site_guide'     => 0,
    'autolinks'          => '百度|https://www.baidu.com/腾讯|https://www.qq.com/',
    'web_site_baidupush' => 0,
    'web_site_getwords'  => 0,
    'site_cache_time'    => 3600,
    'publish_mode'       => 2,
    'category_mode'      => 2,
    'site_url_mode'      => 1,
    'site_category_auth' => 0,
    'data_import'        => 0,
    'data_share'         => 0,
    'icon_mode'          => 1,
    'site'               => 1,
);
